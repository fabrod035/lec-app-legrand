import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import createStore from "./store/createStore";
import * as serviceWorker from "./serviceWorker";

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import "rc-time-picker/assets/index.css";
import "react-toastify/dist/ReactToastify.css";

const initialState = {};
const store = createStore(initialState);
const composeApp = App => (
  <Provider store={store}>
    <App />
  </Provider>
);

const renderApp = () => {
  const App = require("./App").default;
  ReactDOM.render(composeApp(App), document.getElementById("root"));
};

renderApp();

if (module.hot) {
  module.hot.accept("./App", renderApp);
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
