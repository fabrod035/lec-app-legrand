import React from 'react';
import DefaultHeader from './DefaultHeader';
import DefaultFooter from './DefaultFooter';

function DefaultLayout ({children}) {
  return (
    <>
      <DefaultHeader />
      <main className="app-body">
        {children}
      </main>
      <DefaultFooter />
    </>
  )
}

export default DefaultLayout;