import React from "react";

import DefaultLayout from "./DefaultLayout";
import AuthLayout from "./AuthLayout";

export const withMainLayout = Component => props => {
  return (
    <DefaultLayout>
      <Component {...props} />
    </DefaultLayout>
  );
};

export const withAuthLayout = Component => props => {
  return (
    <AuthLayout>
      <Component {...props} />
    </AuthLayout>
  );
};
