let baseUrl;

switch(process.env.REACT_APP_ENV) {
  case "production":
    baseUrl = "https://www.admin.brandsonroad.com";
    break;
  default:
    baseUrl = "http://localhost:1337";
}

export default {
  baseUrl
}