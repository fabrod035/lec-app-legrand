import React, { useState } from "react";
import { Formik } from "formik";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import RegisterForm from "../components/RegisterForm";
import * as yup from "yup";
import config from "../config";
const baseUrl = config.baseUrl;

const RegisterPage = props => {
  const [success, setSuccess] = useState(false);
  return (
    <>
      <ToastContainer />
      <Formik
        initialValues={{username: "", email: "", password: "" }}
        
        validationSchema={yup.object().shape({
          username: yup
          .string()
          .required("User Name is required!"),
          email: yup
            .string()
            .email("Email is invalid")
            .required("Email is required!"),
          password: yup
            .string()
            .min(8, "Password has to be longer than 8 characters!")
            .required("Password is required!"),
        })}
        onSubmit={(values, { setSubmitting, resetForm }) => {
          setSubmitting(true);
          setSuccess(false);
          axios
            .post(baseUrl+"/auth/local/register", { ...values })
            .then(resp => {
              // props.setState({ ...resp.data });
              setSubmitting(false);
              setSuccess(true);
              // values = {username: "", email: "", password: ""};
              resetForm({});
              // props.history.replace("/login");
            })
            .catch(err => {
              console.log(err);
              setSubmitting(false);
              toast("Please enter correct email and password");
            });
        }}
      >
        {p => <RegisterForm {...p} success={success}/>}
      </Formik>
    </>
  );
};
export default RegisterPage;
