import React, { useState, useEffect } from "react";
import { ToastContainer } from "react-toastify";
// import TimePicker from "rc-time-picker";
// import { Row, Col } from "reactstrap";
import MeetingCard from '../components/MeetingCard/MeetingCard'
import { withRouter } from "react-router-dom";
const MeetingPage = ({match}) => {
  const [roomId, setRoomId]=useState('');
  useEffect(()=>{
    setRoomId(match.params.roomId)
  }, [match])
  return (
    <div>
      <ToastContainer />
      <MeetingCard roomId={roomId}/>
    </div>
  );
};

export default withRouter(MeetingPage);
