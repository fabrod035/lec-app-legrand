import React from "react";
import { publicMeetingItems } from "../constants/mock";
import CommonHeroSection from "../components/CommonHeroSection";
import { Link, Redirect } from "react-router-dom";
import { getSession } from "../helpers/session";

const Home = () => {
  const token = getSession().sessionStorage.token;
  return (
    <>
      {
        token && <Redirect to="/meetings" />
      }
      <CommonHeroSection />
      <div className="container" style={{ marginTop: "4rem" }}>
        <div className="row">
          { publicMeetingItems && publicMeetingItems.map((item, index) => {
            return (
              <div className="col-12 col-sm-6 mb-4" key={index}>
                <div className="card">
                  <img
                    className="card-img-bottom"
                    src={item.img}
                    alt=""
                    style={{ width: "100%" }}
                  />
                  <div className="card-body">
                    <Link
                      to={"/meetings/add/"+item.name}
                      className="btn btn-lg btn-block btn-outline-grey"
                      role="button"
                    >
                      {item.name}
                    </Link>
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </>    
  );
};
export default Home;
