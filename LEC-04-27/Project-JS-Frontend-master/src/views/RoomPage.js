import React from "react";
import { ToastContainer } from "react-toastify";
import RoomCard from '../components/RoomCard';
import { withRouter } from "react-router-dom";
const RoomPage = () => {
  return (
    <div>
      <ToastContainer />
      <RoomCard/>
    </div>
  );
};

export default withRouter(RoomPage);
