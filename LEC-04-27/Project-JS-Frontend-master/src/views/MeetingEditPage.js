import React, { useEffect, useState } from "react";
import MeetingCard from '../components/MeetingCard/MeetingCard'
import { withRouter } from "react-router-dom";
import api from "../helpers/api";
const MeetingEditPlayer = ({match}) => {
  const [meeting, setMeeting]=useState(null);
  const [player, setPlayer]=useState("");
  const [isLoading, setIsLoading] = useState(false);
  useEffect(()=>{
    if(match.params.id) {
      setPlayer(match.params.player)
      const fn = async () => {
        try {
          setIsLoading(true);
          const resp = await api.get(`/meetings/${match.params.id}`);
          setMeeting(resp.data);
          setIsLoading(false);
        } catch (err) {
          setIsLoading(false);
        }
      };
      fn();
    }
  }, [match])
  return (
    <div>
      <MeetingCard player={player} meeting={meeting} loading={isLoading}/>
    </div>
  );
};

export default withRouter(MeetingEditPlayer);
