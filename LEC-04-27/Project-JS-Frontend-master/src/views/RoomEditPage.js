import React, { useState, useEffect } from "react";
import { ToastContainer } from "react-toastify";
import RoomCard from '../components/RoomCard';
import { withRouter } from "react-router-dom";
import api from "../helpers/api";
const RoomEditPage = ({match}) => {
  const [room, setRoom]=useState(null);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(()=>{
    if(match.params.id) {
      const fn = async () => {
        try {
          setIsLoading(true);
          const resp = await api.get(`/rooms/${match.params.id}`);
          setRoom(resp.data);
          setIsLoading(false);
        } catch (err) {
          setIsLoading(false);
        }
      };
      fn();
    }
  }, [match])
  return (
    <div>
      <ToastContainer />
      <RoomCard room={room} loading={isLoading}/>
    </div>
  );
};

export default withRouter(RoomEditPage);
