import React from "react";
import { Formik } from "formik";
import axios from "axios";
import { createSession } from "../helpers/session";
import LoginForm from "../components/LoginForm";
import { ToastContainer, toast } from "react-toastify";
import * as yup from "yup";
import {setUserData} from "../modules/auth";
import { connect } from "react-redux";
import config from "../config";
const baseUrl = config.baseUrl;

const mapDispatch = {
  setUserData
}
const Login = props => {
  return (
    <>
      <ToastContainer />
      <Formik
        initialValues={{ identifier: "", password: "" }}
        validationSchema={yup.object().shape({
          identifier: yup
            .string()
            .email("Email is invalid")
            .required("Email is required!"),
          password: yup
            .string()
            .min(8, "Password has to be longer than 8 characters!")
            .required("Password is required!"),
        })}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          axios
            .post(baseUrl+"/auth/local", { ...values })
            .then(resp => {
              if(resp.data.user.confirmed) {
                // props.setState({ ...resp.data });
                props.setUserData({...resp.data})
                createSession(resp);
                props.history.replace("/");
              } else {
                toast("Your account did not allowed yet. Please contact to support team", {type: 'warning'});
              }
              setSubmitting(false);
            })
            .catch(err => {
              setSubmitting(false);
              toast("Please enter correct email and password");
            });
        }}
      >
        {p => <LoginForm {...p} />}
      </Formik>
    </>
  );
};
export default connect(null, mapDispatch) (Login);
