import React, { useEffect, useState } from "react";
import { useHistory, NavLink } from "react-router-dom";
import CommonHeroSection from "../components/CommonHeroSection";
import api from "../helpers/api";
import { getSession } from "../helpers/session";
import config from "../config";
import { Spinner } from "reactstrap";
const baseUrl = config.baseUrl;
const Rooms = () => {
  
  const [rooms, setRooms] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  useEffect(() => {
    const fn = async () => {
      try {
        const userId = getSession().sessionStorage.user.id;
        setIsLoading(true);
        const resp = await api.get("/users/"+userId);
        setRooms(resp.data.rooms);
        setIsLoading(false);
      } catch (err) {
        setIsLoading(false);
      }
    };
    fn();
  }, []);
  const gotoAddPage = () => {
    history.push(`/rooms/add`);
  };
  return (
    <>
      <CommonHeroSection />
      {isLoading && <Spinner />}
      <div className="container" style={{ marginTop: "4rem" }}>
        <div className="d-flex justify-content-start mb-4">
          <button className="btn btn-outline-grey  btn-sm waves-effect waves-light" onClick={gotoAddPage}>
            Add new Room
          </button>
        </div>
        <div className="row">
          { rooms && rooms.map((item, index) => {
            return (
              <div className="col-12 col-sm-4 mb-4" key={index}>
                <div className="card">
                  <img
                    className="card-img-bottom"
                    src={baseUrl + item.image.url}
                    alt=""
                    style={{ width: "100%" }}
                  />
                  <div className="card-body">
                    <div>Title: {item.name}</div>
                    <div>Description: {item.description}</div>
                    <div className="d-flex justify-content-end mt-3">
                      <NavLink
                        to={"/rooms/edit/"+item.id}
                        className="btn btn-sm btn-outline-grey"
                        role="button"
                      >
                        Edit
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </>
  );
};
export default Rooms;
