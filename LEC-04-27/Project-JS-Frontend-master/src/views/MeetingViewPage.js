import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import api from "../helpers/api";
import config from "../config";
import { Spinner } from "reactstrap";
import CommonHeroSection from "../components/CommonHeroSection";
import { ToastContainer } from "react-toastify";
import moment from "moment";
const baseUrl = config.baseUrl;
const MeetingViewPage = ({match}) => {
  const [meeting, setMeeting]=useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const isActive = (item) => {
    const start = new Date(item.startTime);
    const end = new Date(item.endTime);
    const now = new Date();
    if(start <= now && now <= end ) {
      return true;
    }
    return false;
  }
  useEffect(()=>{
    if(match.params.id) {
      const fn = async () => {
        try {
          setIsLoading(true);
          const resp = await api.get(`/meetings/${match.params.id}`);
          setMeeting(resp.data);
          setIsLoading(false);
        } catch (err) {
          setIsLoading(false);
        }
      };
      fn();
    }
  }, [match])
      
  return (
    <>
      <ToastContainer />
      <CommonHeroSection />
      {isLoading && <Spinner />}

      <div className="container ">
        <div className="row">
          <div className="col-md-10 offset-md-1 card p-0">
            {meeting && <div className="card p-0">
              <div style={{ backgroundImage: `url(${
                isActive(meeting) || !meeting.image || !meeting.image.url
                ? "/assets/images/welcome-bg.png"
                : (baseUrl + meeting.image.url)})`,
                  backgroundPosition: "center",
                  backgroundSize: 'cover',
                  minHeight: '350px' }} 
                className="py-5 px-4 text-white text-center">
                <img src="/assets/images/white-logo.png" alt="MDB Logo" className="mb-4" style={{width:"60%", maxWidth: "200px"}}/>
                {isActive(meeting) && <h1 className="mb-3">Greeting and Welcome!</h1>}
                <h2 className="mb-3">{meeting.name}</h2> 
                <div className="mb-3">
                  {
                    moment(meeting.startTime).format("DD-MM-YYYY hh:mm A") +
                            " ~ " +
                    moment(meeting.endTime).format("DD-MM-YYYY hh:mm A")
                  }
                </div>
                <div style={{fontSize: "22px"}}>{meeting.description}</div>
              </div>
            </div>
            }
          </div>
        </div>
      </div>
    </>
  );
};

export default withRouter(MeetingViewPage);
