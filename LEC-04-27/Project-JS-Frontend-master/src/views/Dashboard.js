import React from "react";
import CommonHeroSection from "../components/CommonHeroSection";
import MeetingHistory from "../components/MeetingHistory";
const Dashboard = () => {
  return (
    <>
      <CommonHeroSection />
      <MeetingHistory />
    </>
  );
};
export default Dashboard;
