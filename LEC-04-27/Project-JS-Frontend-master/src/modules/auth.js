import {createAction, handleActions} from "redux-actions";
import {createSelector} from "reselect";
import {SET_USER, CLEAR_USER} from "./actions";

// ==================================
// Selectors
// ==================================
export const userDataSelector = createSelector(
  state => state.auth,
  auth => auth.user
);
export const userTokenSelector = createSelector(
  state => state.auth,
  auth => auth.token
);
// ==================================
// Actions
// ==================================
export const setUserData = createAction(SET_USER);
export const clearUserData = createAction(CLEAR_USER);

// ==================================
// Action Handlers
// ==================================
const ACTION_HANDLERS = {
  [setUserData]: (state, action) => ({
    ...state,
    ...action.payload,
  }),
  [clearUserData]: (state) => ({
    ...state,
    user: null,
    token: null,
  }),
};

// ==================================
// Reducer
// ==================================

const initialState = {
  user: null,
  token: null,
};

export default handleActions(ACTION_HANDLERS, initialState);
