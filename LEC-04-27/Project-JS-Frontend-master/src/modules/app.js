import {createAction, handleActions} from "redux-actions";
import {createSelector} from "reselect";
import {SET_DATA} from "./actions";

// ==================================
// Selectors
// ==================================
export const appDataSelector = createSelector(
  state => state.app,
  app => app.appData
);

export const themeModeSelector = createSelector(
  state => state.app,
  app => app.darkMode
);

export const isSubscriptionActiveSelector = createSelector(
  state => state.app,
  app => (app.appData || {}).subscriptionActive
);

// ==================================
// Actions
// ==================================
export const setAppData = createAction(SET_DATA);

// ==================================
// Action Handlers
// ==================================
const ACTION_HANDLERS = {
  [setAppData]: (state, action) => ({
    ...state,
    appData: {
      musicItems: [],
      videoItems: [],
      ...action.payload,
    },
  }),
};

// ==================================
// Reducer
// ==================================

const initialState = {
  appData: {},
};

export default handleActions(ACTION_HANDLERS, initialState);
