export function createSession(response) {
  sessionStorage.setItem("token", response.data.jwt);
  sessionStorage.setItem("user", JSON.stringify(response.data.user));

  localStorage.setItem("token", response.data.jwt);
  localStorage.setItem("user", JSON.stringify(response.data.user));
}

export function getSession_() {
  return {
    token: localStorage.getItem("token"),
    user: JSON.parse(localStorage.getItem("user"))
  };
}
export function getSession() {
  return {
    sessionStorage: {
      token: sessionStorage.getItem("token"),
      user: JSON.parse(sessionStorage.getItem("user"))
    },
    localStorage: {
      token: localStorage.getItem("token"),
      user: JSON.parse(localStorage.getItem("user"))
    }
  };
}
export function isValidSession() {
  return sessionStorage.getItem("token") || localStorage.getItem("token")
    ? true
    : false;
}

export function destroySession() {
  sessionStorage.removeItem("token");
  sessionStorage.removeItem("user");

  localStorage.removeItem("token");
  localStorage.removeItem("user");
}
