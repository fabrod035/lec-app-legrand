import axios from "axios";
import { getSession } from "./session";
import config from "../config";
const baseUrl = config.baseUrl;

export default {
  get: function(url, params) {
    let getURL = baseUrl + url;

    return axios.get(getURL, {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + getSession().sessionStorage.token
      },
      params
    });
  },
  post: function(url, values) {
    let postURL = baseUrl + url;
    return axios.post(postURL, values, {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + getSession().sessionStorage.token
      }
    });
  },
  patch: function(url, values) {
    let patchURL = baseUrl + url;
    return axios.patch(patchURL, values, {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + getSession().sessionStorage.token
      }
    });
  },
  delete: function(url, values) {
    let deleteURL = baseUrl + url;
    return axios.delete(deleteURL, {
      data: values,
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + getSession().sessionStorage.token
      }
    });
  },
  put: function(url, values) {
    let putURL = baseUrl + url;
    return axios.put(putURL, values, {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + getSession().sessionStorage.token
      }
    });
  }
};
