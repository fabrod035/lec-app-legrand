import React from 'react';
import { BrowserRouter,Route,Switch } from 'react-router-dom';
import { withMainLayout, withAuthLayout } from './containers';
import './assets/styles/main.scss';

import Home from './views/Home';
import LoginPage from './views/LoginPage';
import RegisterPage from './views/RegisterPage';
import ProtectedRoute from './components/ProtectedRoute';
import MeetingPage from './views/MeetingPage';
import MeetingEditPlayer from './views/MeetingEditPage';
import MeetingViewPage from './views/MeetingViewPage';
import Dashboard from './views/Dashboard';
import Rooms from './views/Rooms';
import RoomPage from './views/RoomPage';
import RoomEditPage from './views/RoomEditPage';

function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Switch>
          <Route path="/login" exact component={withAuthLayout(LoginPage)}/>
          <Route path="/register" exact component={withAuthLayout(RegisterPage)}/>
          <ProtectedRoute path="/meetings/add/:roomId" exact component={withMainLayout(MeetingPage)}/>
          <ProtectedRoute path="/meetings/edit/:roomId/:id" exact component={withMainLayout(MeetingEditPlayer)}/>
          <ProtectedRoute path="/meetings/view/:roomId/:id" exact component={withMainLayout(MeetingViewPage)}/>
          <ProtectedRoute path="/meetings" exact component={withMainLayout(Dashboard)}/>  
          <ProtectedRoute path="/rooms/edit/:id" exact component={withMainLayout(RoomEditPage)}/>
          <ProtectedRoute path="/rooms/add" exact component={withMainLayout(RoomPage)}/>
          <ProtectedRoute path="/rooms" exact component={withMainLayout(Rooms)}/>  
          <Route path="/" exact component={withMainLayout(Home)}/>
        </Switch>
      </BrowserRouter>
    </React.Fragment>
  );
}
export default  (App);
