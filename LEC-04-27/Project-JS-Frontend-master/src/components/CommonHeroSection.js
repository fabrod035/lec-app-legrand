import React from "react";

const CommonHeroSection = () => {
  
  return (
    <>
      <div
        className="edge-header"
        style={{ backgroundImage: `url('/assets/images/banner.jpg')` }}
      ></div>
      <div className="container free-bird mb-5">
        <div className="row">
          <div className="col-md-10 mx-auto float-none white z-depth-1 py-2 px-2">
            <div className="card-body">
              <div className="text-center">
                <h2 className="h2-responsive mb-4">Digital Signage Update</h2>
                <p>
                  Please select the area that you would like to update the
                  content of the display(s) to allow for custom messaging.
                </p>
                <i className="fas fa-graduation-cap">
                  <span className="font-weight-bold">
                    How To Documentation
                  </span>
                </i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
    
  );
}

export default CommonHeroSection;