import React from "react";
import {
  Card,
  CardImg,
  CardBody,
} from "reactstrap";
import Modal from './Modal'

const Example = ({button,modalTitle}) => {
  return (
    <div>
      <Card>
        <CardImg
          top
          width="100%"
          src="https://picsum.photos/318/180"
          alt="Card image cap"
        />
        <CardBody>
          {/* <CardTitle>Card title</CardTitle>
          <CardSubtitle>Card subtitle</CardSubtitle>
          <CardText>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </CardText> */}
          <Modal
            size="lg"
            title={modalTitle}
            className="rounded-pill font-weight-light"
            buttonLabel={button}
          />
        </CardBody>
      </Card>
    </div>
  );
};

export default Example;
