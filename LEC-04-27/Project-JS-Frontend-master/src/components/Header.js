import React from "react";
import { Link, useHistory, NavLink } from "react-router-dom";
import { destroySession } from "../helpers/session";
import { userDataSelector, clearUserData } from "../modules/auth";
import { connect } from "react-redux";

const mapState = (state) => ({
  user: userDataSelector(state)
});
const mapDispatch = {
  clearUserData
}
const Navbar = props => {
  const history = useHistory();
  const logout = e => {
    destroySession();
    props.clearUserData();
    history.push('/')
  };
  return (
    <nav className="navbar navbar-dark navbar-expand-md scrolling-navbar unique-color-light">
      <Link className="navbar-brand" to="/">
        <img src="/assets/images/Image_1.png" alt="MDB Logo" />
      </Link>
      <div className="collapse navbar-collapse" id="basicExampleNav">
        <ul className="navbar-nav mx-auto">
          <li className="nav-item">
            <NavLink to="/meetings" activeClassName="active">
              Meetings
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/rooms" activeClassName="active">
              Rooms
            </NavLink>
          </li>
        </ul>
        {props.user && <h6 className="mr-3">Welcome, {props.user.username}</h6>}

        {props.user && 
          <button className="btn btn-outline-grey  btn-sm waves-effect waves-light" onClick={logout}>
            Logout
          </button>
        }
        {!props.user && (
          <button onClick={e => history.push("/login")}
           className="btn btn-outline-grey  btn-sm waves-effect waves-light">
            Login
          </button>
        )}
      </div>
    </nav>
  );
};



export default connect(mapState, mapDispatch)(Navbar);
