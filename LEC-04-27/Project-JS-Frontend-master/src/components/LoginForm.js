import React from "react";
import {Spinner} from "reactstrap";
import { useHistory, Link } from "react-router-dom";
const Login = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting
  /* and other goodies */
}) => {
  const history = useHistory();
  const gotoRegister = () => {
    history.push('/register');
  }
  return (
    <div className="limiter" style={{ backgroundColor: "#666666" }}>
      <div className="container-login100">
        <div className="wrap-login100">
          <form className="login100-form validate-form" onSubmit={handleSubmit}>
            <span className="login100-form-title p-b-43"></span>
        
            <div className="wrap-input100 validate-input mb-3">
              <input
                className={values.identifier?"input100 has-val":"input100"}
                type="text"
                name="identifier"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.identifier}
              />
              <span className="focus-input100">{errors.identifier && touched.identifier && errors.identifier}</span>
              <span className="label-input100">Email</span>
            </div>
            
            <div
              className="wrap-input100 validate-input mb-3"
              data-validate="Password is required"
            >
              <input
                className={values.password?"input100 has-val":"input100"}
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              

              <span className="focus-input100">{errors.password && touched.password && errors.password}</span>
              <span className="label-input100">Password</span>
            </div>

            <div className="flex-sb-m w-full">
              <div className="contact100-form-checkbox mb-3">
                <input
                  className="input-checkbox100"
                  id="ckb1"
                  type="checkbox"
                  name="remember-me"
                />
                <label className="label-checkbox100" htmlFor="ckb1">
                  Remember me
                </label>
              </div>
              <div className="mb-3">
                <Link to="/" className="txt1">
                  Forgot Password?
                </Link>
              </div>
            </div>

            <div className="container-login100-form-btn">
              <button
                type="submit"
                disabled={isSubmitting}
                className="btn btn-lg btn-block btn-outline-grey mb-2"
              >
                {isSubmitting && <Spinner size="sm"/>} Login
              </button>
            </div>

            <div className="text-center p-t-46 p-b-20">
              <span className="txt2" style={{cursor: "pointer"}} onClick={gotoRegister}>or sign up using</span>
            </div>
          </form>

          <div
            className="login100-more"
            style={{ backgroundImage: `url('/assets/images/lec-login.jpg')` }}
          ></div>
        </div>
      </div>
    </div>
  );
};
export default Login;
