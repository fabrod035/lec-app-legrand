import React, { useEffect, useState } from "react";

import * as moment from "moment";
import API from "../helpers/api";

import { useHistory, NavLink } from "react-router-dom";
import { Container } from "reactstrap";
import DataTable from "react-data-table-component";

import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'

import { getSession } from "../helpers/session";
import config from "../config";
const baseUrl = config.baseUrl;

const MeetingHistory = () => {
  const [myEvents, setMyEvents] = useState([])
  const [meetings, setMeetings] = useState([]);
  const [rooms, setRooms] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const columns = [
    // {
    //   name: "Content Player",
    //   selector: "player",
    //   sortable: true,
    // },
    {
      name: "Name",
      selector: "name",
      sortable: true,
    },
    {
      name: "Description",
      selector: "description",
      sortable: true,
    },
    {
      name: "File",
      selector: "file",
      sortable: true,
      cell: (row) => <div>{row.image?row.image.name:''}</div>,
    },
    {
      name: "Start Time",
      selector: "startTime",
      sortable: true,
      cell: (row) => (
        <div>{moment(row.startTime).format("YYYY-MM-DD HH:mm A")}</div>
      ),
    },
    {
      name: "End Time",
      selector: "endTime",
      sortable: true,
      cell: (row) => (
        <div>{moment(row.endTime).format("YYYY-MM-DD HH:mm A")}</div>
      ),
    },
    {
      name: "",
      selector: "name",
      sortable: false,
      cell: (row) => (
        <div className="table-options">
          <button
            className="btn btn-outline-grey  btn-sm waves-effect waves-light mr-2"
            onClick={() => gotoViewPage(row)}
          >
            <i className="fas fa-eye"></i>
          </button>
          <button
            className="btn btn-outline-grey  btn-sm waves-effect waves-light"
            onClick={() => gotoEditPage(row)}
          >
            <i className="fas fa-edit"></i>
          </button>
        </div>
      ),
    },
  ];

  useEffect(() => {
    const fn = async () => {
      try {
        const userId = getSession().sessionStorage.user.id;
        setIsLoading(true);
        const resp = await API.get("/users/"+userId);
        setMeetings(resp.data.meetings);
        let events = resp.data.meetings.map((item)=>{
          return {
            id: item.id,
            groupId: item.room,
            start: item.startTime,
            end: item.endTime,
            title: item.name,
            description: item.description
          }
        })
        setMyEvents(events);
        setRooms(resp.data.rooms);
        setIsLoading(false);
      } catch (err) {
        setIsLoading(false);
      }
    };
    fn();
  }, []);
  const gotoEditPage = (item) => {
    history.push(`/meetings/edit/${item.room}/${item.id}`);
  };
  const gotoViewPage = (item) => {
    history.push(`/meetings/view/${item.room}/${item.id}`);
  };

  const clickEvent = e => {
    console.log(e);
    history.push(`/meetings/view/${e.event.groupId}/${e.event.id}`);
  }
  

  return (
    <>
      <Container>
        <FullCalendar defaultView="dayGridMonth" plugins={[ dayGridPlugin ]} 
          events={myEvents} eventClick={clickEvent}/>

        <DataTable
          title="Meeting List"
          columns={columns}
          data={meetings}
          progressPending={isLoading}
          pagination={true}
          highlightOnHover={true}
          striped={true}
          pointerOnHover={true}
          onRowClicked={gotoEditPage}
        />
        
      </Container>
      <div className="container" style={{ marginTop: "4rem" }}>
        <div className="row">
          { rooms && rooms.map((item, index) => {
            return (
              <div className="col-12 col-sm-4 mb-4" key={index}>
                <div className="card">
                  {item.image && <img
                    className="card-img-bottom"
                    src={baseUrl + item.image.url}
                    alt=""
                    style={{ width: "100%" }}
                  />}
                  <div className="card-body">
                    <NavLink
                      to={"/meetings/add/"+item.id}
                      className="btn btn-lg btn-block btn-outline-grey"
                      role="button"
                    >
                      {item.name}
                    </NavLink>
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </>
  );
};
export default MeetingHistory;
