import React from "react";
import {connect} from "react-redux";
import {Route, Redirect} from "react-router";
import {userTokenSelector} from "../modules/auth";
import { getSession } from "../helpers/session";
import { setUserData } from '../modules/auth'
function ProtectedRoute({setUserData, userToken, component: Component, ...restProps}) {
  const token = getSession().sessionStorage.token;
  if(token && !userToken) {
    setUserData(getSession().sessionStorage);
  }
  return (
    <Route
      {...restProps}
      render={props =>
        token ? (<Component {...props} />) : (
          <Redirect
            to={{
              pathname: "/login",
              state: {from: props.location},
            }}
          />
        )
      }
    />
  );
}

const mapDispatch = {
  setUserData
}
const mapState = state => ({
  userToken: userTokenSelector(state),
});
export default connect(mapState, mapDispatch)(ProtectedRoute);
