/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React, { useState, useEffect } from "react";
import { Row, Col, Input, Spinner } from "reactstrap";
import { toast, ToastContainer } from "react-toastify";
import API from "../../helpers/api";
import FileDropper from "./FileDropper";
import { getSession } from "../../helpers/session";
import { useHistory } from "react-router-dom";
import DateTimeRangeContainer from "react-advanced-datetimerange-picker";
import moment from "moment";
import config from "../../config";
import CommonHeroSection from "../CommonHeroSection";
const baseUrl = config.baseUrl;

let now = new Date();
let start = moment(
  new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0)
);
let end = moment(start).add(1, "days").subtract(1, "seconds");
let ranges = {
  "Today Only": [moment(start), moment(end)],
  "1 Week": [moment(start).subtract(7, "days"), moment(end)],
  "1 Month": [moment(start).subtract(1, "months"), moment(end)],
  "1 Year": [moment(start).subtract(1, "years"), moment(end)],
};
let local = {
  format: "DD-MM-YYYY hh:mm A",
  sundayFirst: true,
};
let maxDate = moment(end).add(24, "hour");

const MeetingCard = ({ roomId, meeting, loading }) => {
  const [imgUrl, setImgUrl] = useState("");
  const [startTime, setStartTime] = useState(moment(new Date()));
  const [endTime, setEndTime] = useState(moment(new Date()));
  const [saving, setSaving] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const history = useHistory();

  const saveMeeting = async (e) => {
    e.preventDefault();
    setSaving(true);
    try {
      const userId = getSession().sessionStorage.user.id;
      const data = {
        name: name,
        description: description,
        startTime: startTime.toDate().getTime(),
        endTime: endTime.toDate().getTime(),
        image: image,
        room: roomId,
        createdBy: userId,
      };
      if (meeting && meeting.id) {
        await API.put(`/meetings/${meeting.id}`, data);
      } else {
        await API.post("/meetings", data);
      }
      setSaving(false);
      history.push("/");
      toast("Saved!");
    } catch (err) {
      setSaving(false);
      toast("Could not save, Try Again! Please make sure server is up.");
    }
  };
  const applyCallback = (startDate, endDate) => {
    setStartTime(startDate);
    setEndTime(endDate);
  };
  useEffect(() => {
    if (meeting) {
      setName(meeting.name);
      setDescription(meeting.description);
      if (meeting.image) {
        setImage(meeting.image.id);
        setImgUrl(meeting.image.url);
      }
      // setName(meeting.name);
      setStartTime(moment(meeting.startTime));
      setEndTime(moment(meeting.endTime));
    }
  }, [meeting]);
  const uploaded = (uploadedImg) => {
    setImage(uploadedImg.id);
    setImgUrl(uploadedImg.url);
  };
  return (
    <div>
      <ToastContainer />
      <CommonHeroSection />
      {loading && <Spinner />}

      <div className="container ">
        <div className="row">
          <div className="col-md-10 offset-md-1 card p-0">
            <div style={{ backgroundImage: `url(${imgUrl? (baseUrl + imgUrl):"/assets/images/uploader.jpg"})`,
                backgroundPosition: "center",
                backgroundSize: 'cover',
                minHeight: '400px' }} 
              className="py-5 px-4 text-white text-center">
              <img src="/assets/images/Image_1.png" alt="MDB Logo" className="mb-3"/>
              <h1 className="mb-3">{name}</h1> 
              <div style={{fontSize: "20px"}}>{description}</div>
            </div>
            <div className="p-4">
              <Row className="my-4">
                <Col xs={4}>Meeting Name: </Col>
                <Col xs={8}>
                  <Input
                    type="text"
                    placeholder=""
                    value={name}
                    onChange={($e) => setName($e.target.value)}
                  />
                </Col>
              </Row>
              <Row className="my-4">
                <Col xs={4}>Greeting Message: </Col>
                <Col xs={8}>
                  <Input
                    type="textarea"
                    placeholder=""
                    value={description}
                    onChange={($e) => setDescription($e.target.value)}
                  />
                </Col>
              </Row>
              <Row className="mb-4">
                <Col xs={4}>Meeting Time: </Col>
                <Col>
                  <DateTimeRangeContainer
                    ranges={ranges}
                    start={startTime}
                    end={endTime}
                    local={local}
                    maxDate={maxDate}
                    applyCallback={applyCallback}
                    smartMode
                  >
                    <Input
                      id="formControlsTextB"
                      type="text"
                      label="Text"
                      placeholder="Enter text"
                      style={{ cursor: "pointer" }}
                      readOnly
                      value={
                        startTime.format("DD-MM-YYYY hh:mm A") +
                        " ~ " +
                        endTime.format("DD-MM-YYYY hh:mm A")
                      }
                    />
                  </DateTimeRangeContainer>
                </Col>
              </Row>
              <Row>
                <Col xs={4}>Screen Content: </Col>
                <Col>
                  <FileDropper onUploaded={uploaded} url={imgUrl}></FileDropper>
                  <button
                    className="btn btn-outline-grey waves-effect waves-light mt-4"
                    style={{ minWidth: "200px" }}
                    onClick={saveMeeting}
                    disabled={saving}
                  >
                    {saving ? (
                      <>
                        <Spinner className="mr-2" size="sm" /> Saving
                      </>
                    ) : (
                      "Save"
                    )}
                  </button>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MeetingCard;
