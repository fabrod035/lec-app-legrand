import React, { useCallback, useState, useEffect } from "react";
import { useDropzone } from "react-dropzone";
import axios from 'axios';
import { CardBody, Card } from "reactstrap";
import config from "../../config";
import { getSession } from "../../helpers/session";
const baseUrl = config.baseUrl;

export default function FileDropzone({onUploaded, url}) {
  const [imgUrl, setImgUrl]=useState("");
  const onDrop = useCallback(async acceptedFiles => {
    // Do something with the files
    var formData = new FormData();
    console.log(acceptedFiles);
    formData.append("files", acceptedFiles[0]);
    const resp = await axios.post(baseUrl+"/upload", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: "Bearer " + getSession().sessionStorage.token
      }
    });
    // toast('Uploaded image successfully')
    setImgUrl(baseUrl+resp.data[0].url);
    onUploaded(resp.data[0]);
  },[onUploaded]);
  useEffect(()=>{
    if(url) {
      setImgUrl(baseUrl+url);
    }
  }, [url])
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <>
      <Card>
        <CardBody className="p-3">
          <div {...getRootProps()} style={{
            padding: "20px",
            border: "dashed 2px",
            borderRadius: "5px",
            display: "flex",
            flexDirection: "column",
            alignItems: "center"
          }}>
            <input {...getInputProps()} />
            {imgUrl && <img src={imgUrl} alt="" style={{
              width: "100%",
              maxWidth: "200px",
              display: "block",
              marginBottom: "20px"
            }}/>}
            <i className="fas fa-cloud-upload-alt mb-3" style={{fontSize: "3rem"}}></i>
            {isDragActive ? (
              <div>Drop the file here ...</div>
            ) : (
              <div>Drag & drop here, or click to select file</div>
            )}
          </div>
        </CardBody>
      </Card>
    </>
  );
}
