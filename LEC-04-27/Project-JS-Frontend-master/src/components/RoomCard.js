/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React, { useState, useEffect } from "react";
import { Row, Col, Input, Spinner } from "reactstrap";
import { toast, ToastContainer } from "react-toastify";
import API from "../helpers/api";
import FileDropper from "./MeetingCard/FileDropper";
import { getSession } from "../helpers/session";
import { useHistory } from "react-router-dom";
import config from "../config";
import CommonHeroSection from "./CommonHeroSection";
const baseUrl = config.baseUrl;

const RoomCard = ({ room, loading }) => {
  const [imgUrl, setImgUrl] = useState("");
  const [saving, setSaving] = useState(false);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const history = useHistory();

  const saveMeeting = async (e) => {
    e.preventDefault();
    setSaving(true);
    try {
      const userId = getSession().sessionStorage.user.id;
      const data = {
        name: name,
        description: description,
        image: image,
        createdBy: userId,
      };
      if (room && room.id) {
        await API.put(`/rooms/${room.id}`, data);
      } else {
        await API.post("/rooms", data);
      }
      setSaving(false);
      history.push("/rooms");
      toast("Saved!");
    } catch (err) {
      setSaving(false);
      toast("Could not save, Try Again! Please make sure server is up.");
    }
  };
  useEffect(() => {
    if (room) {
      setName(room.name);
      setDescription(room.description);
      if (room.image) {
        setImage(room.image.id);
        setImgUrl(room.image.url);
      }
    }
  }, [room]);
  const uploaded = (uploadedImg) => {
    setImage(uploadedImg.id);
    setImgUrl(uploadedImg.url);
  };
  return (
    <div>
      <ToastContainer />
      <CommonHeroSection />
      {loading && <Spinner />}

      <div className="container ">
        <div className="row">
          <div className="col-md-10 offset-md-1 card p-0">
            <div style={{ backgroundImage: `url(${imgUrl? (baseUrl + imgUrl):"/assets/images/uploader.jpg"})`,
                backgroundPosition: "center",
                backgroundSize: 'cover',
                minHeight: '400px' }} 
              className="py-5 px-4 text-white text-center">
            </div>
            <div className="p-4">
              <Row className="my-4">
                <Col xs={4}>Room Title: </Col>
                <Col xs={8}>
                  <Input
                    type="text"
                    placeholder=""
                    value={name}
                    onChange={($e) => setName($e.target.value)}
                  />
                </Col>
              </Row>
              <Row className="my-4">
                <Col xs={4}>Description: </Col>
                <Col xs={8}>
                  <Input
                    type="textarea"
                    placeholder=""
                    value={description}
                    onChange={($e) => setDescription($e.target.value)}
                  />
                </Col>
              </Row>
              <Row>
                <Col xs={4}>Screen Content: </Col>
                <Col>
                  <FileDropper onUploaded={uploaded} url={imgUrl}></FileDropper>
                  <button
                    className="btn btn-outline-grey waves-effect waves-light mt-4"
                    style={{ minWidth: "200px" }}
                    onClick={saveMeeting}
                    disabled={saving}
                  >
                    {saving ? (
                      <>
                        <Spinner className="mr-2" size="sm" /> Saving
                      </>
                    ) : (
                      "Save"
                    )}
                  </button>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RoomCard;
