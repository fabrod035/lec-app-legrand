import React from "react";

const Footer = () => {
  return (
    <footer className="page-footer unique-color-dark">
      <p className="footer-copyright mb-0 py-3 text-center">
        Copyright &copy;{" "}
        <a href="https://tiv.solutions">Totally In View, Inc. </a>
      </p>
    </footer>
  );
};

export default Footer;
