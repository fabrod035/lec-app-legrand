import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import {meetingItems} from '../constants/mock';
import { Spinner, Container, Row, Col, CardHeader, CardFooter } from "reactstrap";

import * as moment from "moment";
import API from "../helpers/api";

import {
  Card as ReactStrapCard,
  CardBody,
} from "reactstrap";
import CommonHeroSection from "./CommonHeroSection";

const Home = () => {
  const [meetings, setMeetings] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();

  useEffect(() => {
    const fn = async () => {
      try {
        setIsLoading(true);
        const resp = await API.get("/meeting");
        setMeetings(resp.data.data);
        setIsLoading(false);
      } catch (err) {
        setIsLoading(false);
      }
    };
    fn();
    
  }, []);
  const gotoMettingHistoryPage =() => {
    history.push(`/meetings`);
  }
  const gotoEditPage = (id) => {
    history.push(`/meetings/edit/${id}`);
  }
  return (
    <>
      <CommonHeroSection />
      <div>
        {isLoading && <Spinner />}
        {meetings.length>0 && (
          <>
            <Container>
              <div className="d-flex align-items-center mt-5">
                <h4 className="mr-3">My Meetings</h4>
                <button className="btn btn-sm btn-outline-grey mr-3" onClick={()=>gotoMettingHistoryPage()}>Show All</button>
              </div>
              <Row>
                {meetings.map((m, i) => {
                  return (
                    <Col key={i} xs="12" sm="6" md="4" className="my-3">
                      <ReactStrapCard>
                        <CardHeader><h5 className="mb-0">Meeting #{i + 1}</h5></CardHeader>
                        <CardBody>
                          <div className="mb-1">
                            <b>Description: </b>
                          </div>
                          <div className="mb-1">
                            <b>Start Time: </b>
                            {moment(m.startTime).format("YYYY-MM-DD HH:mm A")}
                          </div>
                          <div className="mb-3">
                            <b>End Time: </b>
                            {moment(m.endTime).format("YYYY-MM-DD HH:mm A")}
                          </div>
                        </CardBody>
                        <CardFooter>
                          <div className="d-flex justify-content-end">
                            <button className="btn btn-sm btn-outline-grey mr-3" onClick={()=>gotoEditPage(m.id)}>Edit</button>
                            <button className="btn btn-sm btn-outline-grey" >Join</button>
                          </div>
                        </CardFooter>
                      </ReactStrapCard>
                    </Col>
                  );
                })}
              </Row>
              <hr />
            </Container>
          </>
        )}
      </div>
      <div className="container" style={{ marginTop: "4rem" }}>
        <div className="row">
          { meetingItems && meetingItems.map((item, index) => {
            return (
              <div className="col-12 col-sm-4 mb-4" key={index}>
                <div className="card">
                  <img
                    className="card-img-bottom"
                    src={item.img}
                    alt=""
                    style={{ width: "100%" }}
                  />
                  <div className="card-body">
                    <Link
                      to="signage-stairs"
                      className="btn btn-lg btn-block btn-outline-grey"
                      role="button"
                    >
                      {item.name}
                    </Link>
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </>
  );
};

export default Home;
