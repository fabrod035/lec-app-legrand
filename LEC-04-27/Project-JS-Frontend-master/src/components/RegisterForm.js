import React from "react";
import { Spinner } from "reactstrap";
import { useHistory } from "react-router-dom";
const RegisterForm = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  handleSubmit,
  isSubmitting,
  success
  /* and other goodies */
}) => {
  const history = useHistory();
  const gotoLogin = () => {
    history.push("/login");
  };
  return (
    <div className="limiter" style={{ backgroundColor: "#666666" }}>
      <div className="container-login100">
        <div className="wrap-login100">
          <form className="login100-form validate-form" onSubmit={handleSubmit}>
            <span className="login100-form-title p-b-43"></span>

            {success && <div className="alert alert-success" role="alert">
              You are now registered and can log in
            </div>}
            <div className="wrap-input100 validate-input">
              <input
                className={values.username?"input100 has-val":"input100"}
                type="text"
                name="username"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.username}
              />
              <span className="focus-input100">{errors.username && touched.username && errors.username}</span>
              <span className="label-input100">Name</span>
            </div>

            <div className="wrap-input100 validate-input">
              <input
                className={values.email?"input100 has-val":"input100"}
                type="text"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              <span className="focus-input100">{errors.email && touched.email && errors.email}</span>
              <span className="label-input100">Email</span>
            </div>
            
            <div
              className="wrap-input100 validate-input"
              data-validate="Password is required"
            >
              <input
                className={values.password?"input100 has-val":"input100"}
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              <span className="focus-input100">{errors.password && touched.password && errors.password}</span>
              <span className="label-input100">Password</span>
            </div>
            

            <div className="container-login100-form-btn mb-2">
              <button
                type="submit"
                disabled={isSubmitting}
                className="btn btn-lg btn-block btn-outline-grey"
              >
                {isSubmitting && <Spinner size="sm" />} Register
              </button>
            </div>

            <div className="text-center p-t-46 p-b-20">
              <span className="txt2" style={{cursor: "pointer"}} onClick={gotoLogin}>
                or login using
              </span>
            </div>
          </form>

          <div
            className="login100-more"
            style={{ backgroundImage: `url('/assets/images/lec-login.jpg')` }}
          ></div>
        </div>
      </div>
    </div>
  );
};
export default RegisterForm;
