/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React from "react";
import config from "../config";
import { useHistory } from "react-router-dom";
const baseUrl = config.baseUrl;

const ActiveMeetingCard = ({ meeting }) => {
  const history = useHistory();
  const isActive = (item) => {
    const start = new Date(item.startTime);
    const end = new Date(item.endTime);
    const now = new Date();
    if(start <= now && now <= end ) {
      return true;
    }
    return false;
  }
  return (
    <div className="card p-0">
      <div style={{ backgroundImage: `url(${
        isActive(meeting) || !meeting.image || !meeting.image.url
        ? "/assets/images/welcome-bg.png"
        : (baseUrl + meeting.image.url)})`,
          backgroundPosition: "center",
          backgroundSize: 'cover',
          minHeight: '350px' }} 
        className="py-5 px-4 text-white text-center">
        <img src="/assets/images/white-logo.png" alt="MDB Logo" className="mb-4" style={{width:"60%", maxWidth: "200px"}}/>
        {isActive(meeting) && <h1 className="mb-3">Greeting and Welcome!</h1>}
        <h2 className="mb-3">{meeting.name}</h2> 
        <div style={{fontSize: "20px"}}>{meeting.description}</div>
      </div>
      <div className="d-flex justify-content-around p-3">
        <button onClick={e => history.push(`/meetings/view/${meeting.player}/${meeting.id}`)}
          className="btn btn-outline-grey  btn-sm waves-effect waves-light">
          Join
        </button>
        
        <button onClick={e => history.push(`/meetings/edit/${meeting.player}/${meeting.id}`)}
          className="btn btn-outline-grey  btn-sm waves-effect waves-light">
          Edit
        </button>
      </div>
    </div>
  );
};

export default ActiveMeetingCard;
