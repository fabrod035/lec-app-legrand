import {combineReducers} from "redux";
import appReducer from "../modules/app";
import authReducer from "../modules/auth";
import loadingReducer from "../modules/loading";

export const makeRootReducer = asyncReducers => {
  const reducers = {
    app: appReducer,
    auth: authReducer,
    loading: loadingReducer,
    ...asyncReducers,
  };
  return combineReducers(reducers);
};

export default makeRootReducer;
