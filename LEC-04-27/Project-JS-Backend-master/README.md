# Strapi application

A quick description of your strapi application


## mongodb dump/restore

### dump

(mongodump -d <database_name> -o <directory_backup>)

mongodump -d appointment-meeting-db -o dump_db

### restore db

(mongorestore -d <database_name> <directory_backup>)

mongorestore -d appointment-meeting-db dump_db/appointment-meeting-db --drop


